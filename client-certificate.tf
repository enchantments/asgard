resource "vault_pki_secret_backend_sign" "client" {

  backend = "pki"
  name    = "client"

  csr         = tls_cert_request.client.cert_request_pem
  common_name = tls_cert_request.client.subject[0].common_name
}

resource "tls_private_key" "client" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_cert_request" "client" {
  key_algorithm   = "ECDSA"
  private_key_pem = tls_private_key.client.private_key_pem

  subject {
    common_name = "client"
  }

  ip_addresses = [
    jsondecode(data.http.my_ip.body).ip
  ]
}

data "http" "my_ip" {
  url = "https://api.ipify.org?format=json"

  request_headers = {
    "Accept" = "application/json"
  }
}
