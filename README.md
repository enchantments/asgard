# Asgard

An example data center that sets up a control plane and compute pool in GCP.

# Creating the infrastructure

The security of our infrastructure relies in part on mutual TLS authentication. This means we need a client certificate and key to interact with it. Let's get that now:

```
terraform apply -target vault_pki_secret_backend_sign.client
```

It might be convenient to create PKCS12 file for use in your browser:

```
./create-client-certificate.sh
```

That will surface our client certificate and key both in PEM format for scripts and APIs and PKCS12 for browsers, as well as our root certificate, needed by other infrastructure enchantments.

```
ls *.pem *.pfx
client_certificate.pem  client_certificate.pfx  client_private_key.pem  root_certificate.pem
```

We're now ready to create our control plane and initialize its secrets storage:

```
terraform apply -target module.control_plane -target module.vault_initialization
```

Be aware that a Vault initialization produces (or requires the use of) a sensitive file on the local file system called `vault_initial_secrets.json`.

This file will be used in other enchantments to introduce Vault policies, secrets, and integrations in the control plane.

This final step creates the rest of the infrastructure.

```
$ terraform apply
```
