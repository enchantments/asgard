module "vault_initialization" {
  source = "git::https://gitlab.com/enchantments/vault_initialization.git"

  control_plane_ips            = "${module.control_plane.public_ipv4_addresses}"
  control_plane_instance_names = "${module.control_plane.instance_names}"
  vault_address                = "${module.control_plane.vault_address}"
  vault_cacert_path            = "${path.module}/root_certificate.pem"
  vault_client_key_path        = "${path.module}/client_private_key.pem"
  vault_client_cert_path       = "${path.module}/client_certificate.pem"
  ssh_user                     = var.ssh_user
  ssh_private_key              = var.ssh_private_key
}
