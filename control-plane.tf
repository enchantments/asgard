module "control_plane" {
  source = "git::https://gitlab.com/enchantments/control_plane.git"

  region        = var.region
  datacenter    = "gce-${var.gcp_zone}"
  instances     = 3
  dns_zone_name = var.dns_zone_name
  vault_version = var.vault_version
  nomad_version = var.nomad_version

  ssh_user        = var.ssh_user
  ssh_private_key = var.ssh_private_key
  project         = var.gcp_project
  gcp_region      = var.gcp_region
  zone            = var.gcp_zone

  pki_vault_address = var.pki_vault_address
  pki_vault_token   = var.pki_vault_token
  pki_role_name     = "server"
  pki_backend_path  = "pki"
}
