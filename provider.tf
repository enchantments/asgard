provider "vault" {
  address = var.pki_vault_address
  token   = var.pki_vault_token
  ca_cert_file = pathexpand(var.root_ca_certificate_file)
}
