#!/bin/bash

set -e

TERRAFORM_STATE=$(terraform output -json)

echo "${TERRAFORM_STATE}" | jq -r .client_certificate.value > client_certificate.pem
echo "${TERRAFORM_STATE}" | jq -r .client_ca_chain.value    > client_ca_chain.pem
echo "${TERRAFORM_STATE}" | jq -r .client_private_key.value > client_private_key.pem
echo "${TERRAFORM_STATE}" | jq -r .root_certificate.value   > root_certificate.pem

# Verify the private key.
openssl ec -check -pubout -in client_private_key.pem 2>/dev/null
openssl x509 -pubkey -in client_certificate.pem -noout

openssl pkcs12 -macalg sha256 -nodes -chain -export -out client_certificate.pfx -inkey client_private_key.pem -in client_certificate.pem -CAfile client_ca_chain.pem -passout pass:
