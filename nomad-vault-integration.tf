module "nomad_vault_integration" {
  source = "git::https://gitlab.com/enchantments/nomad_vault_integration.git"

  # The following parameters are used for secure introduction of 
  # the Vault token to each Nomad server.

  control_plane_ips = module.control_plane.public_ipv4_addresses
  ssh_user          = var.ssh_user
  ssh_private_key   = var.ssh_private_key

  # The integration needs to be set up in the control plane's
  # Vault cluster.

  vault_address = module.control_plane.vault_address
  token         = jsondecode(file("vault_initial_secrets.json")).root_token
  ca_file       = "${path.module}/root_certificate.pem"
  key_file      = "${path.module}/client_private_key.pem"
  cert_file     = "${path.module}/client_certificate.pem"

  # For this to work, the Vault must have been initialized first.

  vault_initialization_id = module.vault_initialization.initialization_id
}
