output "root_certificate" {
  value = file(pathexpand(var.root_ca_certificate_file))
}

output "client_private_key" {
  value     = tls_private_key.client.private_key_pem
  sensitive = true
}

output "client_certificate" {
  value = vault_pki_secret_backend_sign.client.certificate
}

output "client_ca_chain" {
  value = join("\n", vault_pki_secret_backend_sign.client.ca_chain)
}
