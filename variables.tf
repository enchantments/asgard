variable "dns_zone_name" {}
variable "region" { default = "global" }
variable "ssh_user" {}
variable "ssh_private_key" {}
variable "gcp_project" {}
variable "gcp_region" {}
variable "gcp_zone" {}
variable "intermediate_ca_private_key_file" {}
variable "intermediate_ca_certificate_file" {}
variable "root_ca_certificate_file" {}
variable "pki_vault_address" {}
variable "pki_vault_token" {}

variable "nomad_version" { default = "" }
variable "vault_version" { default = "1.4.0" }
