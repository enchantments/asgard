module "compute_pool" {
  source = "git::https://gitlab.com/enchantments/compute_pool.git"

  region        = var.region
  datacenter    = "gce-${var.gcp_zone}"
  instances     = 2
  nomad_version = var.nomad_version

  control_plane_consul_gossip_key = module.control_plane.consul_gossip_key

  ssh_user        = var.ssh_user
  ssh_private_key = var.ssh_private_key
  project         = var.gcp_project
  zone            = var.gcp_zone

  pki_vault_address = var.pki_vault_address
  pki_vault_token   = var.pki_vault_token
  pki_role_name     = "server"
  pki_backend_path  = "pki"
}
