terraform {
  backend "gcs" {
    bucket = "adrienne-enchantments"
    prefix = "terraform/valkyrie"
  }
}
