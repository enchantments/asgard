#!/bin/bash

set -e

terraform apply -auto-approve -target tls_cert_request.client -target tls_private_key.client -target vault_pki_secret_backend_sign.client
./create-client-certificate.sh
terraform apply -auto-approve -target module.control_plane
terraform apply -auto-approve -target module.vault_initialization
terraform apply -auto-approve -target module.nomad_vault_integration
